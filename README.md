Toolforge disabled tools
========================

Query LDAP and display tools which have been marked as disabled and are now
pending deletion.

Deploy on Toolforge
-------------------
```
$ ssh dev.tooolforge.org
$ become $TOOL_NAME
$ mkdir -p $HOME/www/python
$ git clone https://gitlab.wikimedia.org/toolforge-repos/disabled-tools.git \
	  $HOME/www/python/src
$ touch $HOME/www/python/src/config.yaml
$ chmod u=rw,go= $HOME/www/python/src/config.yaml
$ webservice --backend=kubernetes python3.9 shell
$ python3 -m venv $HOME/www/python/venv
$ source $HOME/www/python/venv/bin/activate
$ python3 -m pip install --upgrade pip wheel
$ python3 -m pip install -r $HOME/www/python/src/requirements.txt
$ cat >> $HOME/www/python/src/config.yaml << EOF
SECRET_KEY: $(python3 -c "import secrets; print(secrets.token_urlsafe(48))")
EOF
$ exit
$ webservice --backend=kubernetes python3.9 start
```

License
-------
[GPL-3.0-or-later](//www.gnu.org/copyleft/gpl.html "GPL-3.0-or-later")
