# -*- coding: utf-8 -*-
#
# This file is part of Toolforge disabled tools
#
# Copyright (C) 2022 Wikimedia Foundation and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import hashlib
import logging
import os
import pwd

import cachelib
import flask
import ldap3
import werkzeug.middleware.proxy_fix
import yaml

import timesince


# Create the Flask application
app = flask.Flask(__name__)
# Add the ProxyFix middleware which reads X-Forwarded-* headers
app.wsgi_app = werkzeug.middleware.proxy_fix.ProxyFix(app.wsgi_app)
# Load configuration from YAML file(s).
# See default_config.yaml for more information
__dir__ = os.path.dirname(__file__)
app.config.update(
    yaml.safe_load(open(os.path.join(__dir__, "default_config.yaml")))
)
try:
    app.config.update(
        yaml.safe_load(open(os.path.join(__dir__, "config.yaml")))
    )
except IOError:
    # It is ok if there is no local config file
    pass


logging.getLogger().addHandler(flask.logging.default_handler)


cache = cachelib.RedisCache(
    host=app.config["REDIS_HOST"],
    key_prefix=hashlib.sha256(
        "{0.pw_name}/{0.pw_dir}".format(pwd.getpwuid(os.getuid())).encode(
            "utf-8"
        )
    ).hexdigest(),
)


EPOCH_DATE = datetime.datetime(2001, 1, 15, tzinfo=datetime.timezone.utc)


def get_disabled_tools(cached=True):
    """Get the list of disabled tools.

    :return: List of maintainer names (cn)
    """
    key = "disabled"
    data = None
    if cached:
        data = cache.get(key)
    if data:
        return data

    cfg = yaml.safe_load(open("/etc/ldap.yaml"))
    conn = ldap3.Connection(cfg["servers"], auto_bind=True, read_only=True)
    base = "ou=servicegroups,{basedn}".format(basedn=cfg["basedn"])
    selector = "(&{})".format(
        "".join(
            [
                "(objectClass=posixAccount)",
                "(pwdPolicySubentry={policy},{basedn})".format(
                    policy="cn=disabled,ou=ppolicies", basedn=cfg["basedn"]
                ),
            ]
        )
    )
    r = conn.extend.standard.paged_search(
        base,
        selector,
        attributes=["cn", "pwdAccountLockedTime"],
        paged_size=256,
        time_limit=5,
        generator=True,
    )
    data = {}
    for tool in r:
        name = tool["attributes"]["cn"][0].removeprefix("tools.")
        data[name] = tool["attributes"]["pwdAccountLockedTime"]
        if data[name] == b"000001010000Z":
            data[name] = EPOCH_DATE
    cache.set(key, data, timeout=300)
    return data


@app.route("/")
def index():
    ctx = {}
    try:
        cached = "purge" not in flask.request.args
        ctx.update({"disabled": get_disabled_tools(cached)})
    except Exception:
        app.logger.exception("Error collecting information for tools")
    return flask.render_template("home.html", **ctx)


@app.template_filter("timesince")
def time_since(d):
    return timesince.timesince(d)


@app.template_filter("days")
def days_since(d):
    return timesince.timesince(d, max_unit="day")


if __name__ == "__main__":
    app.run()
