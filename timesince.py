# -*- coding: utf-8 -*-
#
# This file is part of Toolforge disabled tools
#
# Copyright (C) 2022 Wikimedia Foundation and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
import calendar
import datetime

CHUNKS = (
    (60 * 60 * 24 * 365, "year"),
    (60 * 60 * 24 * 30, "month"),
    (60 * 60 * 24 * 7, "week"),
    (60 * 60 * 24, "day"),
    (60 * 60, "hour"),
    (60, "minute"),
)


def timesince(d, now=None, max_unit="year"):
    """Human readable time elapsed since a given date.

    Adapted from
    https://github.com/django/django/blob/main/django/utils/timesince.py

    >>> then = datetime.datetime(2022, 11, 1)
    >>> now = datetime.datetime(2022, 12, 9)
    >>> timesince(then, now=now)
    '1 month, 1 week'
    >>> timesince(datetime.datetime.now())
    '0 minutes'
    >>> timesince(then, now=now, max_unit="day")
    '38 days'
    """
    now = now or datetime.datetime.now()
    if d.utcoffset() is not None and now.utcoffset() is None:
        now = now.replace(tzinfo=datetime.timezone.utc)
    if now.utcoffset() is not None and d.utcoffset() is None:
        d = d.replace(tzinfo=datetime.timezone.utc)

    delta = now - d
    leapdays = calendar.leapdays(d.year, now.year)
    if leapdays != 0:
        if calendar.isleap(d.year):
            leapdays -= 1
        elif calendar.isleap(now.year):
            leapdays += 1
    delta -= datetime.timedelta(leapdays)

    since = delta.days * 24 * 60 * 60 + delta.seconds
    if since <= 0:
        return "0 minutes"

    min_index = 0
    for i, (seconds, name) in enumerate(CHUNKS):
        if name == max_unit:
            min_index = i

    for i, (seconds, name) in enumerate(CHUNKS):
        count = since // seconds
        if count != 0 and i >= min_index:
            break
    else:
        return "0 minutes"

    parts = []
    depth = 0
    while i < len(CHUNKS) and depth < 2:
        seconds, name = CHUNKS[i]
        count = since // seconds
        if count == 0:
            break
        parts.append(
            "{num:d} {name}{plural}".format(
                num=count,
                name=name,
                plural="s" if count > 1 else "",
            )
        )
        since -= seconds * count
        depth += 1
        i += 1
    return ", ".join(parts)
